﻿
using EmperialApps.WeatherSpark.Agent.Helpers;
using EmperialApps.WeatherSpark.Data;
using System;
using System.ComponentModel;
using System.ServiceModel;

namespace EmperialApps.WeatherSpark.Agent.Internal {

    internal static class Extensions {

        public static void Record( this Exception ex, string footer = "" ) {
            var log = Logger.GetLog( );
            string time = DateTimeOffset.Now.ToString( "o" );
            LittleWatsonAgent.RecordException( ex, "When: " + time, footer, log );
        }

        public static bool WasCancelled( this AsyncCompletedEventArgs e ) {
            return e.Cancelled
                || e.Error is CommunicationObjectAbortedException;
        }

    }

}
