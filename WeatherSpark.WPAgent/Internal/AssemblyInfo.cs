﻿
namespace EmperialApps.WeatherSpark {

    internal static partial class AssemblyInfo {

        public const string Name = "WeatherSpark";

        public const string Title = Name + ".WPAgent";

    }

}
