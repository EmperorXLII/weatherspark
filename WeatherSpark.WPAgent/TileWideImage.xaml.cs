﻿
using EmperialApps.WeatherSpark.Data;
using System.Windows.Controls;
using System.Windows.Data;

namespace EmperialApps.WeatherSpark.Agent {

    /// <summary>Renders the graphs for the Windows Phone wide live tile.</summary>
    public partial class TileWideImage : TileImageBase {

        /// <summary>Initializes a new instance of the <see cref="TileWideImage"/> class.</summary>
        public TileWideImage( ) {
            InitializeComponent( );
            this.ForecastPanel.DataContext = this;
        }


        /// <inheritdoc/>
        public override void DisplayValues( Forecast forecast, Units units, ForecastDisplayMode mode ) {
            // Display component tiles.
            foreach( var tile in new TileImageBase[] { this.MainTile, this.FlipTile } ) {
                tile.Units = units;
                tile.DisplayMode = mode;
                tile.Forecast = forecast;
            }
        }


        /// <inheritdoc/>
        protected override UIElementCollection ForecastPanelChildren {
            get { return this.ForecastPanel.Children; }
        }

    }

}
