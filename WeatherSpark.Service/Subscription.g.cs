﻿
using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace EmperialApps.WeatherSpark.Service {


    public partial class Subscription : TableEntity {

        /// <summary>Initializes a new instance of the <see cref="Subscription"/> class.</summary>
        public Subscription( ) { }


        /// <summary>Gets or sets the notification target URI.</summary>
        public string SerializedChannelUri { get; set; }
        public Uri GetChannelUri( ) { return LoadUri( this.SerializedChannelUri ); }
        public void SetChannelUri( Uri value ) { this.SerializedChannelUri = SaveUri( value ); }


        /// <summary>Updates the members of the current item with the values in the specified item.</summary>
        public static bool Update( Subscription current, Subscription item ) {
            if( current == null || item == null )
                return false;

            bool updated = false;

            var channelUri = item.SerializedChannelUri ?? current.SerializedChannelUri;
            if( current.SerializedChannelUri != channelUri ) {
                current.SerializedChannelUri = channelUri;
                updated = true;
            }

            return updated;
        }


        /// <inheritdoc/>
        public override string ToString( ) {
            string format = "{0}: Location={1}, Device={2}";
            if( this.SerializedChannelUri != null )
                format += ", URI={3:u}";

            return string.Format( format,
                this.GetType( ).Name, this.PartitionKey, this.RowKey,
                this.GetChannelUri( )
            );
        }

    }

}

