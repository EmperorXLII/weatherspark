﻿
using System;
using System.ServiceModel;

namespace EmperialApps.WeatherSpark.Service {

    /// <summary>Represents a subscription service for tracking weather updates.</summary>
    [ServiceContract]
    public interface IWeatherService {

        /// <summary>Adds a subscription for the specified location.</summary>
        [OperationContract]
        string Subscribe( Guid deviceId, double latitude, double longitude, string channelUri );

        /// <summary>Adds a subscription for the specified location.</summary>
        [OperationContract]
        string Subscribe2( Guid deviceId, double latitude, double longitude, string preferredUrl, string channelUri );

        /// <summary>Removes a subscription for the specified location.</summary>
        [OperationContract]
        string Unsubscribe( Guid deviceId, double latitude, double longitude );

        /// <summary>Marks the specified location as in use.</summary>
        [OperationContract]
        bool Use( double latitude, double longitude );

    }

}
