
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Diagnostics;
using System.IO;

namespace EmperialApps.WeatherSpark.Service {

    public class AzureLocalStorageTraceListener : XmlWriterTraceListener {

        private const string ServiceLogName = "WeatherSpark.Service.svclog";

        public AzureLocalStorageTraceListener( )
            : base( Path.Combine( AzureLocalStorageTraceListener.GetLogDirectory( ), ServiceLogName ) ) {
        }

        public static string GetLogDirectory( ) {
            string logDirectory = RoleEnvironment.GetLocalResource( ServiceLogName ).RootPath;
            return logDirectory;
        }

    }

}
