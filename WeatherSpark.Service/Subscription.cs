
using EmperialApps.WeatherSpark.Data;
using EmperialApps.WeatherSpark.Service.Internal;
using System;

namespace EmperialApps.WeatherSpark.Service {

    /// <summary>Represents a weather notification subscription.</summary>
    public partial class Subscription {

        /// <summary>Initializes a new instance of the <see cref="Subscription"/> class with the specified location and device id.</summary>
        public Subscription( Coordinate location, Guid deviceId )
            : base( location.GetName( ), deviceId.ToString( "D" ) ) {
        }

        /// <summary>Initializes a new instance of the <see cref="Subscription"/> class with the specified location, device id, and channel URI.</summary>
        public Subscription( Coordinate location, Guid deviceId, Uri channelUri )
            : this( location, deviceId ) {
            this.SetChannelUri( channelUri );
        }


        /// <summary>Gets a value indicating whether the subscription has a valid channel URI.</summary>
        public bool HasChannelUri {
            get { return !string.IsNullOrEmpty( this.SerializedChannelUri ); }
        }


        private static string SaveUri( Uri uri ) {
            return uri == null
                 ? null
                 : uri.ToString( );
        }

        private static Uri LoadUri( string uri ) {
            return uri == null
                 ? null
                 : new Uri( uri, UriKind.Absolute );
        }

    }

}
