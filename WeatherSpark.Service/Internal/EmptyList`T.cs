﻿
using System;
using System.Collections;
using System.Collections.Generic;

namespace EmperialApps.WeatherSpark.Service.Internal {

    /// <summary>Represents a read-only list that ignores added elements.</summary>
    internal sealed class EmptyList<T> : IList<T> {

        public static readonly IList<T> Instance = new EmptyList<T>( );

        private EmptyList( ) { }

        public T this[int index] {
            get { throw new ArgumentOutOfRangeException( "index" ); }
            set { throw new ArgumentOutOfRangeException( "index" ); }
        }

        public int Count {
            get { return 0; }
        }

        public bool IsReadOnly {
            get { return true; }
        }

        public void Add( T item ) { }

        public void Clear( ) { }

        public bool Contains( T item ) {
            return false;
        }

        public void CopyTo( T[] array, int arrayIndex ) { }

        public int IndexOf( T item ) {
            return -1;
        }

        public void Insert( int index, T item ) { }

        public bool Remove( T item ) { return false; }

        public void RemoveAt( int index ) { }

        public IEnumerator<T> GetEnumerator( ) {
            yield break;
        }

        IEnumerator IEnumerable.GetEnumerator( ) {
            return GetEnumerator( );
        }

    }

}
