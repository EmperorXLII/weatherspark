﻿
using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace EmperialApps.WeatherSpark.Service {


    public partial class ForecastInfo : TableEntity {

        /// <summary>Initializes a new instance of the <see cref="ForecastInfo"/> class.</summary>
        public ForecastInfo( ) { }


        /// <summary>Gets or sets the description for the forecast.</summary>
        public string Description { get; set; }

        /// <summary>Gets or sets the URL of the forecast file.</summary>
        public string ForecastUrl { get; set; }

        /// <summary>Gets or sets the source URL of the yr.no forecast, if any.</summary>
        public string YrnoSourceUrl { get; set; }

        /// <summary>Gets or sets the download status of the NOAA forecast.</summary>
        public Int32 NoaaDownloadStatus { get; set; }

        /// <summary>Gets or sets the last time the location was processed by a scheduler.</summary>
        public string SerializedLastProcessed { get; set; }
        public DateTimeOffset GetLastProcessed( ) { return LoadDateTimeOffset( this.SerializedLastProcessed ); }
        public void SetLastProcessed( DateTimeOffset value ) { this.SerializedLastProcessed = SaveDateTimeOffset( value ); }

        /// <summary>Gets or sets the last time a user requested the location.</summary>
        public string SerializedLastUserRequest { get; set; }
        public DateTimeOffset GetLastUserRequest( ) { return LoadDateTimeOffset( this.SerializedLastUserRequest ); }
        public void SetLastUserRequest( DateTimeOffset value ) { this.SerializedLastUserRequest = SaveDateTimeOffset( value ); }

        /// <summary>Gets or sets the threshold for the next scheduled download of the location.</summary>
        public string SerializedNextDownloadThreshold { get; set; }
        public DateTimeOffset GetNextDownloadThreshold( ) { return LoadDateTimeOffset( this.SerializedNextDownloadThreshold ); }
        public void SetNextDownloadThreshold( DateTimeOffset value ) { this.SerializedNextDownloadThreshold = SaveDateTimeOffset( value ); }


        /// <summary>Updates the members of the current item with the values in the specified item.</summary>
        public static bool Update( ForecastInfo current, ForecastInfo item ) {
            if( current == null || item == null )
                return false;

            bool updated = false;

            var description = item.Description ?? current.Description;
            if( current.Description != description ) {
                current.Description = description;
                updated = true;
            }

            var forecastUrl = item.ForecastUrl ?? current.ForecastUrl;
            if( current.ForecastUrl != forecastUrl ) {
                current.ForecastUrl = forecastUrl;
                updated = true;
            }

            var yrnoSourceUrl = item.YrnoSourceUrl ?? current.YrnoSourceUrl;
            if( current.YrnoSourceUrl != yrnoSourceUrl ) {
                current.YrnoSourceUrl = yrnoSourceUrl;
                updated = true;
            }

            var noaaDownloadStatus = item.NoaaDownloadStatus;
            if( current.NoaaDownloadStatus != noaaDownloadStatus ) {
                current.NoaaDownloadStatus = noaaDownloadStatus;
                updated = true;
            }

            var lastProcessed = item.SerializedLastProcessed ?? current.SerializedLastProcessed;
            if( current.SerializedLastProcessed != lastProcessed ) {
                current.SerializedLastProcessed = lastProcessed;
                updated = true;
            }

            var lastUserRequest = item.SerializedLastUserRequest ?? current.SerializedLastUserRequest;
            if( current.SerializedLastUserRequest != lastUserRequest ) {
                current.SerializedLastUserRequest = lastUserRequest;
                updated = true;
            }

            var nextDownloadThreshold = item.SerializedNextDownloadThreshold ?? current.SerializedNextDownloadThreshold;
            if( current.SerializedNextDownloadThreshold != nextDownloadThreshold ) {
                current.SerializedNextDownloadThreshold = nextDownloadThreshold;
                updated = true;
            }

            return updated;
        }


        /// <inheritdoc/>
        public override string ToString( ) {
            string format = "{0}: Location={1},{2}";
            if( this.SerializedLastUserRequest != null )
                format += ", LastRequest={3:u}";
            if( this.SerializedNextDownloadThreshold != null )
                format += ", NextDownload={4:u}";
            if( this.SerializedLastProcessed != null )
                format += ", Processed={5:u}";
            if( this.ForecastUrl != null )
                format += ", URL={6}";
            if( this.YrnoSourceUrl != null )
                format += ", YRNO={7}";

            return string.Format( format,
                this.GetType( ).Name, this.PartitionKey, this.RowKey,
                this.GetLastUserRequest( ),
                this.GetNextDownloadThreshold( ),
                this.GetLastProcessed( ),
                this.ForecastUrl,
                this.YrnoSourceUrl
            );
        }

    }

}

