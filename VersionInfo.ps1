
trap { exit 1 }

$name = 'VersionInfo.cs'
$directory = Split-Path -Parent $MyInvocation.MyCommand.Definition
$destination = Join-Path $directory $name
$date = (Get-Date).Date

if( (Test-Path $destination) -and (Get-Item $destination).LastWriteTime.Date -eq $date ) {
  Write-Host "$name up to date."
  return
}
else {
  Write-Host "Updating $name to $($date.ToShortDateString())..."
}

@"

namespace EmperialApps.WeatherSpark {

    internal static class VersionInfo {

        public const string Year = "$($date.Year)";
        public const string Date = "$($date.ToString( 'MMdd' ))";

    }

}
"@ | Out-File $destination -Encoding UTF8
