﻿
using System.Collections.Generic;

namespace EmperialApps.WeatherSpark {

    public interface IAssertion<T> : IEqualityComparer<T> {

        void Assert( T actual, T expected );

    }

}
