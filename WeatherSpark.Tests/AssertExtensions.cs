﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmperialApps.WeatherSpark {

    public static class AssertExtensions {

        public static IOrderedEnumerable<T> Sort<T>( this IEnumerable<T> collection )
            where T : IComparable<T> {
            return collection.OrderBy( t => t );
        }

        public static void AssertAll<T>( this T actual, T expected, params IAssertion<T>[] assertions ) {
            var failedAssertions = new Dictionary<int, Exception>( );

            for( int i = 0; i < assertions.Length; ++i )
                try {
                    assertions[i].Assert( actual, expected );
                }
                catch( Exception ex ) {
                    failedAssertions.Add( i, ex );
                }

            // If multiple assertions failed, report all failures.
            if( failedAssertions.Count > 1 )
                throw new AggregateException(
                    string.Join( Environment.NewLine, failedAssertions.Values.Select( ex => ex.Message ) ),
                    failedAssertions.Values );
            // Otherwise, re-assert any single failure.
            else
                foreach( int key in failedAssertions.Keys )
                    assertions[key].Assert( actual, expected );
        }

    }

}
