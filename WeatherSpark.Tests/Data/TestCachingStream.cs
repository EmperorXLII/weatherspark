﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;
using Xunit.Extensions;

namespace EmperialApps.WeatherSpark.Data {

    public class TestCachingStream {

        [Fact]
        public void ReadByte_AfterPeekByte_ReturnsCachedValue( ) {
            const int length = 3;
            using( var stream = CreateStream( length ) )
            using( var cached = CreateCachedStream( length ) ) {
                byte[] values = stream.ToArray( );
                int[] expected = new int[]{
                    values[0],
                    values[0],
                    values[1],
                    values[2],
                    values[2],
                    values[2],
                    -1,
                    -1
                };

                int[] actual = new int[] {
                    cached.PeekByte(),
                    cached.ReadByte(),
                    cached.ReadByte(),
                    cached.PeekByte(),
                    cached.PeekByte(),
                    cached.ReadByte(),
                    cached.PeekByte(),
                    cached.ReadByte()
                };

                Assert.Equal( expected, actual );
            }
        }

        [Fact]
        public void Read_AfterPeekByte_ReturnsCachedValue( ) {
            const int length = 10;
            using( var stream = CreateStream( length ) )
            using( var cached = CreateCachedStream( length ) ) {
                byte[] expected = stream.ToArray( );

                byte[] actual = new byte[expected.Length];
                cached.PeekByte( );
                cached.Read( actual, 0, length / 2 );
                cached.Read( actual, length / 2, length / 2 );

                Assert.Equal( expected, actual );
            }
        }

        [Theory]
        [InlineData( 0 )]
        [InlineData( 1 )]
        [InlineData( 2 )]
        public void Seek_GivenValidValue_BehavesLikeUnderlyingStream( int seed ) {
            var random = new Random( seed );
            int length = random.Next( 10, 100 );
            int iterations = random.Next( 10, 20 );
            using( var stream = CreateStream( length ) )
            using( var cached = CreateCachedStream( length ) ) {
                for( int i = 0; i < iterations; ++i ) {
                    var origin = _seekOrigins[random.Next( _seekOrigins.Length )];
                    int offset = GetSeekOffset( random, origin, length, (int)stream.Position );
                    long expected = stream.Seek( offset, origin );

                    long actual = cached.Seek( offset, origin );

                    bool success = expected == actual && cached.Position == stream.Position;
                    if( !success ) {
                        string message = string.Format( "[{0}] Seek difference: expected {1} ({2}), actual {3} ({4}).", i, expected, stream.Position, actual, cached.Position );
                        Assert.True( success, message );
                    }
                }
            }
        }


        #region Utility

        private static readonly SeekOrigin[] _seekOrigins = (SeekOrigin[])Enum.GetValues( typeof( SeekOrigin ) );

        private static int GetSeekOffset( Random random, SeekOrigin origin, int length, int position ) {
            switch( origin ) {
                case SeekOrigin.Begin:
                    return random.Next( 0, length );
                case SeekOrigin.Current:
                    return random.Next( 0, length - position );
                case SeekOrigin.End:
                    return random.Next( -length, 0 );

                default:
                    throw new ArgumentOutOfRangeException( "Unrecognized seek origin: " + origin );
            }
        }

        private static CachingStream CreateCachedStream( int length ) {
            return new CachingStream( CreateStream( length ) );
        }

        private static MemoryStream CreateStream( int length ) {
            byte[] buffer = new byte[length];
            for( int i = 0; i < length; ++i )
                buffer[i] = unchecked( (byte)i );

            return new MemoryStream( buffer );
        }

        #endregion

    }

}
