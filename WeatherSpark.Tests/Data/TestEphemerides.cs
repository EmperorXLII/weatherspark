﻿
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Extensions;

namespace EmperialApps.WeatherSpark.Data {

    public class TestEphemerides {

        private const double H = 60;
        private static readonly DateTimeOffset DateM7 = new DateTimeOffset( 2012, 8, 12, 0, 0, 0, TimeSpan.FromHours( -7 ) );
        private static readonly DateTimeOffset DateM6 = new DateTimeOffset( 2012, 8, 12, 0, 0, 0, TimeSpan.FromHours( -6 ) );

        private static double Days( DateTimeOffset date ) {
            TimeSpan offset = date - default( DateTimeOffset );
            return offset.TotalDays;
        }

        private static DateTimeOffset SecondsToNone( DateTimeOffset date ) {
            return date
                .AddSeconds( -date.Second )
                .AddMilliseconds( -date.Millisecond );
        }

        private static TimeSpan Time( int hours, int minutes ) {
            return Time( 0, hours, minutes );
        }

        private static TimeSpan Time( int days, int hours, int minutes ) {
            int totalHours = days * ConvertValue.HoursPerDay + hours;
            return new TimeSpan( totalHours, minutes, 0 );
        }


        private const string JulianDateValues_Name = "JulianDateValues";
        public static IEnumerable<object[]> JulianDateValues {
            get {
                yield return new object[] { DateM7, 2456151.79 };
                yield return new object[] { new DateTimeOffset( 2000, 1, 1, 12, 0, 0, default( TimeSpan ) ), 2451545.0 };
            }
        }

        [Theory]
        [PropertyData( JulianDateValues_Name )]
        public void GetJulianDate_GivenValidDate_ReturnsExpectedResult( DateTimeOffset gregorian, double julian ) {
            double actual = Ephemerides.GetJulianDate( gregorian );

            Assert.Equal( julian, actual, 2 );
        }


        private const string DaylightValues_Name = "DaylightValues";
        public static IEnumerable<object[]> DaylightValues {
            get {
                yield return new object[] { new Coordinate( 40.02, -105.28 ), DateM7, DateM7.AddHours( 5 + 10 / H ), DateM7.AddHours( 19 + 1 / H ) };
                yield return new object[] { new Coordinate( 30.27, -97.74 ), DateM6, DateM6.AddHours( 5 + 56 / H ), DateM6.AddHours( 19 + 15 / H ) };
            }
        }

        [Theory]
        [PropertyData( DaylightValues_Name )]
        public void CalculateDaylight_GivenValidValues_ReturnsExpectedResult( Coordinate location, DateTimeOffset date, DateTimeOffset sunrise, DateTimeOffset sunset ) {
            var expected = Pair.Create( sunrise, sunset );

            var daylight = Ephemerides.CalculateDaylight( location, date );
            var actual = Pair.Create(
                SecondsToNone( daylight.Item1 ),
                SecondsToNone( daylight.Item2 ) );

            Assert.Equal( expected, actual );
        }

        [Theory]
        [InlineData( 71.27, -156.80, -9.0 )]
        [InlineData( -55.94, -67.28, -3.5 )]
        [InlineData( -77.85, 166.67, +12.0 )]
        public void CalculateDaylight_GivenValuesWithExtremeLatitude_DoesNotThrow( double latitude, double longitude, double offset ) {
            var location = new Coordinate( latitude, longitude );
            var date = DateTimeOffset.Now;

            Assert.DoesNotThrow( ( ) => Ephemerides.CalculateDaylight( location, date ) );
        }


        private const string PhaseValues_Name = "PhaseValues";
        public static IEnumerable<object[]> PhaseValues {
            get {
                yield return new object[] { new DateTimeOffset( 1979, 02, 26, 16, 0, 0, TimeSpan.FromHours( +0 ) ), 0.00, 0.00 };
                // http://eclipse.gsfc.nasa.gov/SKYCAL/SKYCAL.html
                yield return new object[] { new DateTimeOffset( 2015, 01, 04, 23, 0, 0, TimeSpan.FromHours( -7 ) ), 1.00, 0.50 };
                yield return new object[] { new DateTimeOffset( 2015, 10, 12, 19, 0, 0, TimeSpan.FromHours( -6 ) ), 0.00, 0.00 };
                yield return new object[] { new DateTimeOffset( 2020, 06, 13, 12, 0, 0, TimeSpan.FromHours( +6 ) ), 0.50, 0.75 };
                yield return new object[] { new DateTimeOffset( 2020, 12, 22, 06, 0, 0, TimeSpan.FromHours( +7 ) ), 0.50, 0.25 };
            }
        }

        [Theory]
        [PropertyData( PhaseValues_Name )]
        public void CalculateMoonPhase_GivenValidValues_ReturnsExpectedResult( DateTimeOffset date, double expectedPhase, double expectedAge ) {
            var expected = Tuple.Create( expectedPhase, expectedAge );

            double age, phase = Ephemerides.CalculateMoonPhase( date, out age );
            var actual = Tuple.Create(
                Math.Round( phase, 2 ),
                Math.Round( age, 2 ) % 1.0 );

            Assert.Equal( expected, actual );
        }


        private const string TransitValues_Name = "TransitValues";
        public static IEnumerable<object[]> TransitValues {
            get {
                var Boston = new Coordinate( 42.37, -71.05 );
                yield return new object[] { Boston, new DateTimeOffset( 1986, 03, 06, 0, 0, 0, TimeSpan.FromHours( -5 ) ), 1, new[] { Time( 04, 21 ), Time( 13, 08 ) } };
                // http://aa.usno.navy.mil/data/docs/RS_OneDay.php
                yield return new object[] { Boston, new DateTimeOffset( 2015, 01, 04, 0, 0, 0, TimeSpan.FromHours( -5 ) ), 1, new[] { Time( -1, 15, 31 ), Time( 06, 22 ), Time( 16, 24 ), Time( 1, 07, 07 ) } };
                yield return new object[] { Boston, new DateTimeOffset( 2015, 10, 12, 0, 0, 0, TimeSpan.FromHours( -4 ) ), 2, new[] { Time( 06, 25 ), Time( 18, 08 ), Time( 1, 07, 21 ), Time( 1, 18, 38 ) } };

                var Austin = new Coordinate( 30.27, -97.74 );
                yield return new object[] { Austin, new DateTimeOffset( 2017, 05, 21, 0, 0, 0, TimeSpan.FromHours( -5 ) ), 1, new[] { Time( 03, 42 ), Time( 15, 59 ) } };
                yield return new object[] { Austin, new DateTimeOffset( 2017, 11, 11, 0, 0, 0, TimeSpan.FromHours( -6 ) ), 1, new[] { Time( 00, 23 ), Time( 13, 50 ) } };

                // https://www.timeanddate.com/moon/australia/sydney
                var Sydney = new Coordinate( -33.87, 151.21 );
                yield return new object[] { Sydney, new DateTimeOffset( 2020, 06, 12, 0, 0, 0, TimeSpan.FromHours( +10 ) ), 3, new[] { Time( -1, 22, 12 ), Time( 11, 54 ), Time( 23, 09 ), Time( 1, 12, 24 ), Time( 2, 00, 05 ), Time( 2, 12, 52 ) } };
                yield return new object[] { Sydney, new DateTimeOffset( 2020, 06, 13, 0, 0, 0, TimeSpan.FromHours( +10 ) ), 1, new[] { Time( -1, 23, 09 ), Time( 12, 24 ) } };
                yield return new object[] { Sydney, new DateTimeOffset( 2020, 12, 12, 0, 0, 0, TimeSpan.FromHours( +11 ) ), 1, new[] { Time( 03, 32 ), Time( 17, 10 ) } };

                // https://www.timeanddate.com/moon/uk/london
                var London = new Coordinate( 51.51, -0.13 );
                yield return new object[] { London, new DateTimeOffset( 2017, 06, 24, 0, 0, 0, TimeSpan.FromHours( +1 ) ), 2, new[] { Time( 0, 05, 23 ), Time( 0, 21, 31 ), Time( 1, 06, 28 ), Time( 1, 22, 24 ) } };
                yield return new object[] { London, new DateTimeOffset( 2017, 06, 24, 0, 0, 0, TimeSpan.FromHours( +1 ) ), 9, new[] {
                    Time( 0, 05, 23 ), Time( 0, 21, 31 ),
                    Time( 1, 06, 28 ), Time( 1, 22, 24 ),
                    Time( 2, 07, 39 ), Time( 2, 23, 07 ),
                    Time( 3, 08, 54 ), Time( 3, 23, 42 ),
                    Time( 4, 10, 08 ), Time( 5, 00, 11 ),
                    Time( 5, 11, 20 ), Time( 6, 00, 36 ),
                    Time( 6, 12, 30 ), Time( 7, 00, 59 ),
                    Time( 7, 13, 37 ), Time( 8, 01, 22 ),
                    Time( 8, 14, 43 ), Time( 9, 01, 45 )
                } };
            }
        }

        [Theory]
        [PropertyData( TransitValues_Name )]
        public void CalculateMoonTransits_GivenValidValues_ReturnsExpectedResult( Coordinate location, DateTimeOffset date, int days, TimeSpan[] expected ) {
            var actual = new List<TimeSpan>( );
            foreach( var pair in Ephemerides.CalculateMoonTransits( location, date, days ) ) {
                DateTimeOffset moonrise, moonset;
                pair.GetValues( out moonrise, out moonset );
                actual.Add( moonrise - date );
                actual.Add( moonset - date );
            }

            // Verify all values are within an acceptable tolerance.
            double riseTolerance = expected.Length > 7 ? 0.035 : 0.025;
            double setTolerance = expected.Length == 2 ? riseTolerance : 5 * riseTolerance;
            int failures = 0;
            int count = Math.Max( actual.Count, expected.Length );
            var message = new StringBuilder( "Expected Actual  (Error)" ).AppendLine( );
            for( int i = 0; i < count; ++i ) {
                message.AppendFormat( "[{0}]", i );
                TimeSpan expectedTime, actualTime;
                if( AppendTime( message, expected, i, out expectedTime )
                  & AppendTime( message, actual, i, out actualTime ) ) {
                    double tolerance = i % 2 == 0 ? riseTolerance : setTolerance;
                    double difference = expectedTime.TotalHours - actualTime.TotalHours;
                    if( Math.Abs( difference ) > tolerance ) {
                        message.AppendFormat( "  ({0:0.000})", difference );
                        ++failures;
                    }
                }
                else {
                    message.Append( "  (!)" );
                    ++failures;
                }

                message.AppendLine( );
            }

            message.AppendFormat( "Failures: {0}", failures );
            Assert.True( failures == 0, message.ToString( ) );
        }

        private static bool AppendTime( StringBuilder message, IList<TimeSpan> times, int index, out TimeSpan time ) {
            if( index < times.Count ) {
                time = times[index];
                string sign = time < TimeSpan.Zero ? "-" : "+";
                message.AppendFormat( " {0}{1:hh':'mm}", sign, time );
                return true;
            }

            message.Append( "   —  " );
            time = default( TimeSpan );
            return false;
        }

    }

}
