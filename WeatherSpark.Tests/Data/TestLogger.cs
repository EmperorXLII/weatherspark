﻿
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Xunit;
using Xunit.Extensions;

namespace EmperialApps.WeatherSpark.Data {

    public class TestLogger {

        [Fact]
        public void GetLog_AfterLogCleared_ReturnsEmptyCollection( ) {
            Logger.ClearLog( );

            var actual = Logger.GetLog( );

            Assert.Empty( actual );
        }

        [Theory]
        [InlineData( 1 )]
        [InlineData( Logger.Capacity / 2 )]
        [InlineData( Logger.Capacity )]
        [InlineData( Logger.Capacity * 3 / 2 )]
        [InlineData( Logger.Capacity * 3 )]
        public void GetLog_AfterMultipleCallsToLog_ReturnsAllMessagesWithinCapacity( int count ) {
            Logger.ClearLog( );
            int offset = Math.Max( 0, count - Logger.Capacity );
            string[] expected = new string[Math.Min( count, Logger.Capacity )];

            for( int i = 0; i < count; ++i ) {
                string message = i.ToString( );
                Logger.Log( typeof( TestLogger ), message );

                int index = i - offset;
                if( index >= 0 )
                    expected[index] = string.Format( "{0:000} {1}: {2}", 1 + i, typeof( TestLogger ).Name, message );
            }

            var actual = Logger.GetLog( ).Select( StripContextElements );

            Assert.Equal( expected, actual );
        }


        #region Utility

        private static readonly Regex _matchContextElements = new Regex( @"\[[^]]+\]\s" );

        private static string StripContextElements( string message ) {
            return _matchContextElements.Replace( message, "" );
        }

        #endregion

    }

}
