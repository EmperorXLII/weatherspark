
using System.IO;
using Xunit;

namespace EmperialApps.WeatherSpark.Data {

    public class TestXmlSanitizingStream {

        [Fact]
        public void ReadOnlyReturnsLegalXmlCharacters( ) {
            // This should be stripped to "\t\r\n<>:"
            string xml = "\0\t\a\r\b\n<>:";

            // Load the XML as a Stream
            using( var buffer = new MemoryStream( System.Text.Encoding.Default.GetBytes( xml ) ) ) {
                using( var sanitizer = new XmlSanitizingStream( buffer ) ) {
                    Assert.Equal( sanitizer.Read( ), '\t' );
                    Assert.Equal( sanitizer.Read( ), '\r' );
                    Assert.Equal( sanitizer.Read( ), '\n' );
                    Assert.Equal( sanitizer.Read( ), '<' );
                    Assert.Equal( sanitizer.Read( ), '>' );
                    Assert.Equal( sanitizer.Read( ), ':' );
                    Assert.Equal( sanitizer.Read( ), -1 );
                    Assert.True( sanitizer.EndOfStream );
                }
            }

            using( var buffer = new MemoryStream( System.Text.Encoding.Default.GetBytes( xml ) ) ) {
                using( var sanitizer = new XmlSanitizingStream( buffer ) ) {
                    Assert.Equal( sanitizer.ReadToEnd( ), "\t\r\n<>:" );
                }
            }

        }

    }

}
