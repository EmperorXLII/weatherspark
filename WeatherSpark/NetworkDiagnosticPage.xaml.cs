﻿
using EmperialApps.WeatherSpark.Data;
using EmperialApps.WeatherSpark.Internal;
using EmperialApps.WeatherSpark.Resources;
using Microsoft.Phone.BackgroundTransfer;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Threading;


namespace EmperialApps.WeatherSpark {

    public partial class NetworkDiagnosticPage : PhoneApplicationPage {

        private readonly List<DiagnosticState> _diagnosticResults = new List<DiagnosticState>( );
        private readonly DispatcherTimer _diagnosticTimeout;
        private int _timeouts;


        public NetworkDiagnosticPage( ) {
            typeof( NetworkDiagnosticPage ).Log( "Initializing" );
            InitializeComponent( );

            this._diagnosticTimeout = new DispatcherTimer { Interval = TimeSpan.FromSeconds( 1 ) };
            this._diagnosticTimeout.Tick += OnDiagnosticTimeoutTick;
        }

        protected override void OnNavigatedTo( NavigationEventArgs e ) {
            base.OnNavigatedTo( e );

            this.Dispatcher.BeginInvoke( this.PerformNetworkDiagnostics );
        }

        protected override void OnNavigatedFrom( NavigationEventArgs e ) {
            typeof( NetworkDiagnosticPage ).Log( "Leaving" );
            base.OnNavigatedFrom( e );

            this.CancelPendingNetworkDiagnostics( );
        }


        private void PerformNetworkDiagnostics( ) {
            this.SetDiagnosticStatus( "Performing network diagnostic..." );
            this._diagnosticResults.Clear( );
            this.UpdateProgressIndicator( );

            var diagnostics = new Dictionary<string, string> {
                { "small static file", Settings.Corrections },
                { "small web page", "http://www.example.com/" },
                { "geonames result page", "http://api.geonames.org/findNearbyPlaceName?username=weatherspark&style=FULL&lat=26.62&lng=-81.83" },
                { "NOAA forecast file", "https://forecast.weather.gov/MapClick.php?lat=26.62&lon=-81.83&FcstType=digitalDWML" },
            };
            foreach( var pair in diagnostics ) {
                string name = pair.Key;
                Uri address = new Uri( pair.Value );
                PerformNetworkDiagnostic( name + " [WebClient]", address, this.BeginWebClientDiagnostic, this.CancelWebClientDiagnostic );
                PerformNetworkDiagnostic( name + " [WebRequest]", address, this.BeginWebRequestDiagnostic, this.CancelWebRequestDiagnostic );
                PerformNetworkDiagnostic( name + " [BackgroundTransfer]", address, this.BeginBackgroundTransferDiagnostic, this.CancelBackgroundTransferDiagnostic );
            }

            this.UpdateProgressIndicator( );
        }

        private void PerformNetworkDiagnostic( string name, Uri address, Action<DiagnosticState> beginDiagnostic, Action<DiagnosticState> cancelDiagnostic ) {
            int id = this._diagnosticResults.Count;
            var state = new DiagnosticState( id, name, address, cancelDiagnostic );
            this._diagnosticResults.Add( state );

            this.SetDiagnosticStatus( string.Format( "Starting network diagnostic #{0} ({1} - {2})...", id, name, address ) );
            beginDiagnostic( state );
        }

        private void OnNetworkDiagnosticComplete( DiagnosticState state, Exception exception, long result ) {
            this.SetDiagnosticStatus( string.Format( "Complete network diagnostic #{0} ({1} - {2}): {3}", state.Id, state.Name, state.Address, result ) );
            state.Complete( exception, result );
            this._timeouts = 0;

            if( exception != null )
                exception.Record( "diagnostic #" + state.Id );

            bool finished = this.UpdateProgressIndicator( );
            if( finished )
                this.OnNetworkDiagnosticsCompleted( );
        }

        private void OnDiagnosticTimeoutTick( object sender, EventArgs e ) {
            this._timeouts += 1;

            bool finished = this.UpdateProgressIndicator( );
            if( finished && this.CancelPendingNetworkDiagnostics( ) )
                this.OnNetworkDiagnosticsCompleted( );
        }

        private bool CancelPendingNetworkDiagnostics( ) {
            bool anyCancelled = false;
            foreach( var state in this._diagnosticResults ) {
                bool cancelled = state.Cancel( );
                anyCancelled |= cancelled;
            }

            return anyCancelled;
        }


        private void SetDiagnosticStatus( string status ) {
            typeof( NetworkDiagnosticPage ).Log( status );
            this.DiagnosticLog.Items.Add( status );
        }

        private bool UpdateProgressIndicator( ) {
            if( this._diagnosticResults.Count == 0 ) {
                this.ProgressIndicator.Value = 0;
                return false;
            }


            const double MaximumProgress = 100.0;
            const double ProgressGroupCount = 3;
            const double ProgressPerGroup = MaximumProgress / ProgressGroupCount;

            const int MaximumTimeouts = 6;

            int finishedCount = this._diagnosticResults.Count( r => r.IsCompleted );
            bool finished =
                   finishedCount == this._diagnosticResults.Count
                || this._timeouts > MaximumTimeouts;

            double totalProgress =
                finished
                    ? MaximumProgress
                    : ProgressPerGroup + (ProgressPerGroup * finishedCount / this._diagnosticResults.Count)
                                       + (ProgressPerGroup * this._timeouts / MaximumTimeouts);
            this.ProgressIndicator.Value = totalProgress;

            return finished;
        }

        private void OnNetworkDiagnosticsCompleted( ) {
            this.DiagnosticLog.Items.Clear( );
            this.ShowDiagnosticLog( );
            this.Dispatcher.BeginInvoke( SendDiagnosticLog );
        }

        private void ShowDiagnosticLog( ) {
            this.SendDiagnosticLogButton.Visibility = Visibility.Visible;
            this.DiagnosticLog.ItemsSource = Logger.GetLog( );
            this.DiagnosticLog.Focus( );
        }

        private void OnSendDiagnosticLogClicked( object sender, RoutedEventArgs e ) {
            SendDiagnosticLog( );
        }

        private static void SendDiagnosticLog( ) {
            new Exception( "[diagnostic log]" ).Report( Localized.ErrorSource.Debug );
        }

        #region Diagnostics

        private void BeginWebClientDiagnostic( DiagnosticState state ) {
            var client = new WebClient( );
            state.Tag = client;

            client.OpenReadCompleted += ( c, e ) => {
                var s = (DiagnosticState)e.UserState;
                if( e.Cancelled )
                    return;

                e.Result.WaitForMissingResponse( typeof( NetworkDiagnosticPage ) );
                using( e.Result )
                    this.OnNetworkDiagnosticComplete( s, e.Error, e.Error != null ? -1 : e.Result.Length );
            };

            client.OpenReadAsync( state.Address, state );
        }

        private void CancelWebClientDiagnostic( DiagnosticState state ) {
            var client = (WebClient)state.Tag;
            client.CancelAsync( );
        }


        private void BeginWebRequestDiagnostic( DiagnosticState state ) {
            var request = WebRequest.CreateHttp( state.Address );
            state.Tag = request;

            request.BeginGetResponse( ar => {
                var s = (DiagnosticState)ar.AsyncState;

                long resultLength;
                Exception error;
                try {
                    error = null;
                    using( var response = request.EndGetResponse( ar ) )
                    using( var result = response.GetResponseStream( ) )
                    using( var reader = new StreamReader( result ) )
                        resultLength = reader.ReadToEnd( ).Length;
                }
                catch( WebException ex ) {
                    error = ex;
                    resultLength = (long)ex.Status;
                }
                catch( Exception ex ) {
                    error = ex;
                    resultLength = -1;
                }

                if( !s.IsCancelled )
                    this.Dispatcher.BeginInvoke( new Action<DiagnosticState, Exception, long>( this.OnNetworkDiagnosticComplete ), s, error, resultLength );
            }, state );
        }

        private void CancelWebRequestDiagnostic( DiagnosticState state ) {
            var request = (WebRequest)state.Tag;
            request.Abort( );
        }


        private void BeginBackgroundTransferDiagnostic( DiagnosticState state ) {
            Uri downloadLocation = new Uri( "/shared/transfers/NetworkDiagnostic" + state.Id + ".tmp", UriKind.Relative );
            DeleteDiagnostic( downloadLocation.OriginalString );

            var request = new BackgroundTransferRequest( state.Address, downloadLocation );
            request.TransferStatusChanged += ( s, e ) => {
                if( e.Request.TransferStatus != TransferStatus.Completed )
                    return;

                using( e.Request ) {
                    if( !state.IsCancelled )
                        this.OnNetworkDiagnosticComplete( state, e.Request.TransferError, e.Request.BytesReceived );
                }

                DeleteDiagnostic( e.Request.DownloadLocation.OriginalString );
            };

            new TransferMonitor( request ).RequestCancel( );

            var monitor = new TransferMonitor( request );
            state.Tag = monitor;

            monitor.Failed += ( s, e ) => {
                if( !state.IsCancelled )
                    this.OnNetworkDiagnosticComplete( state, e.Request.TransferError, -1 );
            };
            monitor.RequestStart( );
        }

        private void CancelBackgroundTransferDiagnostic( DiagnosticState state ) {
            var monitor = (TransferMonitor)state.Tag;
            monitor.RequestCancel( );
        }

        private static void DeleteDiagnostic( string path ) {
            using( var store = IsolatedStorageFile.GetUserStoreForApplication( ) )
                store.DeleteFile( path );
        }


        private sealed class DiagnosticState {
            private Action<DiagnosticState> _cancel;

            public DiagnosticState( int id, string name, Uri address, Action<DiagnosticState> cancel ) {
                this.Id = id;
                this.Name = name;
                this.Address = address;
                this._cancel = cancel;
            }

            public int Id { get; private set; }
            public string Name { get; private set; }
            public Uri Address { get; private set; }
            public long? Result { get; private set; }
            public Exception Exception { get; private set; }

            public object Tag { get; set; }

            public bool IsCancelled { get { return this._cancel == null; } }
            public bool IsCompleted { get { return this.Result.HasValue || this.IsCancelled; } }


            public void Complete( Exception exception, long result ) {
                this.Exception = exception;
                this.Result = result;
            }

            public bool Cancel( ) {
                if( this.IsCompleted )
                    return false;

                this._cancel( this );
                this._cancel = null;
                return true;
            }

            public override string ToString( ) {
                string exception = this.Exception == null ? null : " [" + this.Exception.GetType( ).Name + "] " + this.Exception;
                string status = this.Result.HasValue
                              ? this.Result.ToString( )
                              : this.IsCancelled
                              ? "Cancelled"
                              : "Pending";
                return string.Format( "Result #{0} ({1} - {2}): {3}{4}", this.Id, this.Name, this.Address, status, exception );
            }
        }

        #endregion

    }

}
