SETUP FOR LITTLE WATSON
=======================

In app.xaml.cs add the following to Application_UnhandledException

    LittleWatson.ReportException(e.ExceptionObject, DateTime.Now.ToString());
    
and the following to Application_Launching

    LittleWatson.CheckForPreviousException();


SETUP FOR METROGRIDHELPER
=========================

In app.xaml.cs add the following to the constructor, in the section
if (System.Diagnostics.Debugger.IsAttached)

    MetroGridHelper.IsVisible = true;

SETUP FOR WP7TombstoneHelper
============================

In each page you want to persist data, add the following

    protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
    {
       this.SaveState(e); 
    }
 
    protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
    {
       this.RestoreState();
    }