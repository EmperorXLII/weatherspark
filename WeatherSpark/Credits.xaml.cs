﻿
using EmperialApps.WeatherSpark.Resources;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace EmperialApps.WeatherSpark {

    /// <summary>Represents a credit displayed by the <see cref="Credits"/> control.</summary>
    public class Credit {
        public string Title { get; set; }
        public string Description { get; set; }
        public Uri Link { get; set; }
    }

    /// <summary>Represents a collection of objects.</summary>
    public class ObjectCollection : ObservableCollection<object> {
        public string Group { get; set; }
    }

    /// <summary>Displays the credits in the About page.</summary>
    public partial class Credits : UserControl {

        public Credits( ) {
            InitializeComponent( );

            var creditGroups =
                from property in typeof( AppResources ).GetProperties( BindingFlags.Public | BindingFlags.Static )
                where property.Name.StartsWith( "Credits" )
                   && property.Name.EndsWith( "description" )
                let parts = property.Name.Split( '_' )
                let category = parts[1]
                let creditId = parts[2][0]
                orderby category[0], creditId
                group creditId by category;

            var credits = new ObjectCollection( );
            foreach( var creditGroup in creditGroups ) {
                string categoryBaseId = "Credits_" + creditGroup.Key + "_";
                string header = AppResources.ResourceManager.GetString( categoryBaseId + "header" );
                var collection = new ObjectCollection { Group = header };
                foreach( var creditId in creditGroup ) {
                    string baseId = categoryBaseId + creditId;
                    string link = AppResources.ResourceManager.GetString( baseId + "link" );
                    string title = AppResources.ResourceManager.GetString( baseId + "title" );
                    string description = AppResources.ResourceManager.GetString( baseId + "description" );
                    collection.Add( new Credit {
                        Link = string.IsNullOrWhiteSpace( link ) ? null : new Uri( link ),
                        Title = title,
                        Description = description
                    } );
                }

                credits.Add( collection );
            }

            this.CreditsList.ItemsSource = credits;
            this.Loaded += this.OnLoaded;
        }

        private void OnLoaded( object sender, RoutedEventArgs e ) {
            // Fix size to parent scroll viewer, so that inner list box will scroll properly.
            ScrollViewer parent = null;
            for( FrameworkElement p = this; parent == null && p != null; p = p.Parent as FrameworkElement )
                parent = p as ScrollViewer;

            if( parent != null )
                this.MaxHeight = parent.ActualHeight;
        }

    }

    /// <summary>Returns visible for non-null input.</summary>
    public sealed class NullToVisibilityConverter : IValueConverter {

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture ) {
            return value == null
                 ? Visibility.Collapsed
                 : Visibility.Visible;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture ) {
            return DependencyProperty.UnsetValue;
        }

    }

}
