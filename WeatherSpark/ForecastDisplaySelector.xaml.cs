﻿
using EmperialApps.WeatherSpark.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace EmperialApps.WeatherSpark {

    /// <summary>Allows selection of different levels of detail for a forecast.</summary>
    public partial class ForecastDisplaySelector : UserControl {

        /// <summary>Initializes a new instance of the <see cref="ForecastDisplaySelector"/> class.</summary>
        public ForecastDisplaySelector( ) {
            if( DesignerProperties.IsInDesignTool )
                return;

            InitializeComponent( );

            foreach( var selector in this.Selectors )
                selector.DisplayLevelChanged += this.OnSelectorDisplayLevelChanged;
        }

        /// <summary>Sets the selected and supported display levels.</summary>
        public void SetDisplayLevel( ForecastDisplayLevel displayLevel, ForecastDisplayLevel supportedMask, ForecastDisplayLevel baseMask ) {
            var unsupportedRows = new List<int>( );
            foreach( var selector in this.Selectors ) {
                ForecastDisplayLevel mask = selector.Mask;
                ForecastDisplayLevel baseSupport = mask & baseMask;
                ForecastDisplayLevel selectorSupport = baseSupport & supportedMask;
                bool summaryAndDetails = ((int)baseSupport & ((int)baseSupport >> 1)) != 0;

                if( selectorSupport == 0 )
                    unsupportedRows.Add( Grid.GetRow( selector ) );

                selector.IsEnabled = selectorSupport != 0;
                selector.Limit = summaryAndDetails ? 1.0 : 0.5;
                selector.DisplayLevel = displayLevel & selectorSupport;
            }

            foreach( int row in unsupportedRows )
                LayoutRoot.RowDefinitions[row].Height = new GridLength( 0.0 );

            foreach( FrameworkElement child in LayoutRoot.Children ) {
                int row = Grid.GetRow( child );
                if( unsupportedRows.Contains( row ) )
                    child.Visibility = Visibility.Collapsed;
            }
        }


        private IEnumerable<DisplaySelector> Selectors {
            get { return this.LayoutRoot.Children.OfType<DisplaySelector>( ); }
        }

        private void OnMinimumDisplayLevelChanged( ) {
            ForecastDisplayLevel level = this.DisplayLevel;
            this.OnSelectorDisplayLevelChanged( this, new RoutedPropertyChangedEventArgs<ForecastDisplayLevel>( level, level ) );
        }

        private void OnSelectorDisplayLevelChanged( object sender, RoutedPropertyChangedEventArgs<ForecastDisplayLevel> e ) {
            ForecastDisplayLevel oldLevel = this.DisplayLevel;
            ForecastDisplayLevel newLevel = oldLevel;

            // Remove old value from level and add new.
            newLevel &= ~e.OldValue;
            newLevel |= e.NewValue | this.MinimumDisplayLevel;

            if( newLevel != oldLevel ) {
                this.DisplayLevel = newLevel;
                this.OnDisplayLevelChanged( new RoutedPropertyChangedEventArgs<ForecastDisplayLevel>( oldLevel, newLevel ) );
            }
        }

        private void OnMoonReferenceSizeChanged( object sender, SizeChangedEventArgs e ) {
            // Double effective size of moon selector, so that Summary hits "Visual" display level, bypassing "Text" display level.
            this.MoonSelector.Margin = new Thickness { Right = -e.NewSize.Width };
        }

    }

}
