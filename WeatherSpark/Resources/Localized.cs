﻿
using System.Globalization;


namespace EmperialApps.WeatherSpark.Resources {

    /// <summary>Provides access to string resources.</summary>
    public class Localized {

        private static AppResources _strings = new AppResources( );

        public AppResources Strings { get { return _strings; } }


        public static string Format( string format, FormatArgument args ) {
            return args.Format( format );
        }

        public static string Format( string format, params object[] args ) {
            FormatArgument formatArgs = args;
            return Format( format, formatArgs );
        }


        public enum ErrorSource {
            Download,
            Loading,
            Locations,
            TileGeneration,
            Unknown,
            Debug,
        }

        public enum Button {
            add,
            backward,
            follow,
            forward,
            refresh,
        }

        public enum Menu {
            about,
            delete,
            purchase,
            settings,
            view,
        }

        public enum Popup {
            DeleteForecast,
            LocationServiceDisabled,
            LocationServiceNoData,
            LocationUnknown,
            ProblemReport,
            RebuildTiles,
            ServerDownloadError,
            TrialLimitReached,
        }

        public enum Status {
            Complete = 0,

            DebugSettingUpdated,
            DebugLogUpdated,
            DebugForecastSaved,

            AlreadySubscribed,
            Cancelling,
            ContactingServer,
            Downloading,
            DownloadRequestFailure,
            ForecastFound,
            LocationFound,
            LocationServiceDisabled,
            LocationServiceInitializing,
            LocationServiceNoData,
            LocationServiceReady,
            LocationsNearby,
            LocationsNearbyUnavailable,
            LocationUnknown,
            NetworkError,
            PersistentError,
            RepairingForecast,
            SearchError,
            Searching,
            StartingLocationService,
            TransientError,
            UnexpectedError,
            UpdatingForecast,

            SearchFound,
            SearchFound0 // (base for number-specific values)
        }


        /// <summary>Combines primary status and secondary log format string arguments.</summary>
        public struct FormatArgument {
            public readonly object Status;
            public readonly object Log;

            private FormatArgument( object status, object log ) {
                this.Status = status;
                this.Log = log;
            }

            public FormatArgument StatusArgument {
                get { return new FormatArgument( this.Status, null ); }
            }

            public FormatArgument LogArgument {
                get { return new FormatArgument( null, this.Log ); }
            }

            public static implicit operator FormatArgument( string status ) {
                return new FormatArgument( status, null );
            }

            public static implicit operator FormatArgument( object[] status ) {
                return new FormatArgument( status, null );
            }

            public FormatArgument AddLogArgument( object log ) {
                return new FormatArgument( this.Status, log );
            }

            public string Format( string format ) {
                object[] args =
                       this.Status as object[]
                    ?? this.Log as object[]
                    ?? new[] { this.Status ?? this.Log };

                string message =
                    args[0] == null
                        ? format
                        : string.Format( CultureInfo.CurrentCulture, format, args );
                return message;
            }


            public override string ToString( ) {
                string status = ToString( this.Status );
                string log = ToString( this.Log );

                string result;
                if( status == null && log == null )
                    result = "Empty";
                else if( status != null && log != null )
                    result = "Status: '" + status + "'; Log: '" + log + "'";
                else if( status != null )
                    result = "Status: '" + status + "'";
                else
                    result = "Log: '" + log + "'";

                return result;
            }

            private static string ToString( object arg ) {
                if( arg == null )
                    return null;

                object[] args = arg as object[];
                if( args != null )
                    return string.Join( "', '", args );

                return arg.ToString( );
            }
        }

    }
}
