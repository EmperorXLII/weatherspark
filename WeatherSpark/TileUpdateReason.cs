﻿
using System;

namespace EmperialApps.WeatherSpark {

    /// <summary>Indicates the reason for a tile update request.</summary>
    public enum TileUpdateReason {
        Refresh = 0,
        CreateTile,
        SettingChange,
    }

}
