﻿
using System.Resources;

[assembly: NeutralResourcesLanguage( "en-US" )]

namespace EmperialApps.WeatherSpark {

    internal static partial class AssemblyInfo {

        public const string Title = "WeatherSpark";

    }

}
