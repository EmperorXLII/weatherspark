﻿
using System;

namespace EmperialApps.WeatherSpark.Internal {

    /// <summary>Indicates the reason for a forecast download.</summary>
    public enum DownloadReason {
        AutoRefresh = 0,
        ManualRefresh,
        Subscription,
        Reset,
    };

}
