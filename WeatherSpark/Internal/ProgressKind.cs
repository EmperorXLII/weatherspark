﻿
using System;

namespace EmperialApps.WeatherSpark.Internal {

    /// <summary>Indicates the kind of progress being reported.</summary>
    [Flags]
    internal enum ProgressKind {
        Incomplete = 0,

        Complete = 1 << 0,
        Important = 1 << 1,
        Error = 1 << 2,

        CompletedSuccessfully = Complete | Important,
        CompletedUnsuccessfully = Complete | Important | Error,
    }

}
