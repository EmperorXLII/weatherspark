﻿
using EmperialApps.WeatherSpark.Agent;
using EmperialApps.WeatherSpark.Data;
using EmperialApps.WeatherSpark.Internal;
using EmperialApps.WeatherSpark.Resources;
using System;
using System.ComponentModel;
using System.Windows.Media;

namespace EmperialApps.WeatherSpark {

    /// <summary>Represents a live tile setting.</summary>
    public sealed class TileOption : INotifyPropertyChanged {

        /// <summary>Initializes a new instance of the <see cref="TileOption"/> class.</summary>
        public TileOption( )
            : this( DesignerProperties.IsInDesignTool ? null : Settings.Current, null ) { }

        /// <summary>Initializes a new instance of the <see cref="TileOption"/> class with the specified settings.</summary>
        public TileOption( LocationSettings settings, Forecast forecast ) {
            this._settings = settings;
            this._forecast = forecast;
            if( this._settings != null )
                this._settings.PropertyChanged += this.OnSettingChanged;
        }


        /// <summary>Gets the location settings for the option.</summary>
        public LocationSettings LocationSettings { get { return this._settings ?? Settings.Current; } }

        /// <summary>Gets the forecast for the option.</summary>
        public Forecast Forecast {
            get { return this._forecast; }
            set {
                if( this._forecast != value ) {
                    this._forecast = value;
                    this.OnPropertyChanged( ForecastArgs );
                }
            }
        }

        /// <summary>Gets the display mode for the option.</summary>
        public ForecastDisplayMode DisplayMode {
            get { return this._displayMode; }
            set {
                if( this._displayMode != value ) {
                    this._displayMode = value;
                    this.OnPropertyChanged( ImageSourceArgs );
                    this.OnPropertyChanged( DisplayModeTitleArgs );
                }
            }
        }

        /// <summary>Gets the title text for the display mode of the tile.</summary>
        public string DisplayModeTitle {
            get {
                string id = "SettingsPage_Tile_LiveTile_" + this._displayMode;
                return AppResources.ResourceManager.GetString( id );
            }
        }

        /// <summary>Gets the URI for the tile image.</summary>
        public Uri ImageSource {
            get {
                return this._displayMode == ForecastDisplayMode.None
                     ? SharedStorage.BackgroundImage
                     : null;
            }
        }

        /// <summary>Gets the background color for the tile.</summary>
        public Color BackgroundColor {
            get {
                return this._displayMode == ForecastDisplayMode.None
                     ? default( Color )
                     : Display.ToColor( Settings.TileBackgroundColorValue );
            }
        }

        /// <summary>Gets the brush for current value visuals.</summary>
        public Brush CurrentValuesBrush {
            get { return Display.ToBrush( Settings.TileCurrentValueColor ); }
        }

        /// <summary>Gets the brush for predicted value visuals.</summary>
        public Brush PredictedValuesBrush {
            get {
                return this._displayMode == ForecastDisplayMode.None
                     ? Display.ToBrush( Display.White )
                     : Display.ToBrush( Settings.TilePredictedValueColor );
            }
        }

        /// <summary>Gets the title text for the tile.</summary>
        public string TileTitle {
            get { return this.LocationSettings.GetTileTitle( this._displayMode ); }
        }

        /// <summary>Gets the title text for the enabled state of the tile.</summary>
        public string EnabledTitle {
            get {
                return this._displayMode == ForecastDisplayMode.None
                     ? AppResources.SettingsPage_Tile_LiveTile_FlipDisabled
                     : AppResources.SettingsPage_Tile_LiveTile_FlipEnabled;
            }
        }


        /// <inheritdoc/>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>Updates the background color for the tile.</summary>
        public void RefreshColors( ) {
            this.OnPropertyChanged( BackgroundColorArgs );
            this.OnPropertyChanged( CurrentValuesBrushArgs );
            this.OnPropertyChanged( PredictedValuesBrushArgs );
        }


        #region Private Members

        private static readonly PropertyChangedEventArgs PredictedValuesBrushArgs = new PropertyChangedEventArgs( "PredictedValuesBrush" );
        private static readonly PropertyChangedEventArgs CurrentValuesBrushArgs = new PropertyChangedEventArgs( "CurrentValuesBrush" );
        private static readonly PropertyChangedEventArgs DisplayModeTitleArgs = new PropertyChangedEventArgs( "DisplayModeTitle" );
        private static readonly PropertyChangedEventArgs BackgroundColorArgs = new PropertyChangedEventArgs( "BackgroundColor" );
        private static readonly PropertyChangedEventArgs ImageSourceArgs = new PropertyChangedEventArgs( "ImageSource" );
        private static readonly PropertyChangedEventArgs TileTitleArgs = new PropertyChangedEventArgs( "TileTitle" );
        private static readonly PropertyChangedEventArgs ForecastArgs = new PropertyChangedEventArgs( "Forecast" );

        private readonly LocationSettings _settings;
        private ForecastDisplayMode _displayMode;
        private Forecast _forecast;


        private void OnSettingChanged( object sender, PropertyChangedEventArgs e ) {
            if( this._displayMode == ForecastDisplayMode.None )
                return;

            switch( e.PropertyName ) {
                case LocationSettings.SearchPropertyName:
                case LocationSettings.TileTitlePropertyName:
                    OnPropertyChanged( TileTitleArgs );
                    break;
            }
        }

        private void OnPropertyChanged( PropertyChangedEventArgs e ) {
            var handler = this.PropertyChanged;
            if( handler != null )
                handler( this, e );
        }

        #endregion
    }

}
