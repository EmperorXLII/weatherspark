﻿
using EmperialApps.WeatherSpark.Internal;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace EmperialApps.WeatherSpark {

    /// <summary>Holds the view state for the current search selection.</summary>
    public partial class SearchStatus {

        /// <summary>Represents the action a user can perform during a search.</summary>
        public enum UserAction {
            Cancel = 0,
            Save,
            Refresh,
            Minus,
        }


        /// <summary>Gets the image used to identify the current <see cref="Action"/>.</summary>
        public ImageBrush ActionImage { get { return this._actionImage; } }


        #region Private Members

        private static readonly Dictionary<UserAction, BitmapImage> _actionImages = new Dictionary<UserAction, BitmapImage>( );
        private readonly ImageBrush _actionImage = new ImageBrush { Stretch = Stretch.Fill };

        partial void InitializeInstance( ) {
            this.UpdateActionImage( );
        }

        partial void ActionChanged( UserAction oldValue ) {
            this.UpdateActionImage( );
        }

        private void UpdateActionImage( ) {
            BitmapImage bitmap;
            UserAction action = this.Action;
            if( !_actionImages.TryGetValue( action, out bitmap ) ) {
                string actionName = action.ToString( ).ToLowerInvariant( );
                Uri imageUri = actionName.ToImageUri( );
                _actionImages[action] = bitmap = new BitmapImage( imageUri ) { CreateOptions = BitmapCreateOptions.None };
            }

            this._actionImage.ImageSource = bitmap;
        }

        #endregion

    }

}
