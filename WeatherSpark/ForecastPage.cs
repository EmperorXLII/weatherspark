﻿
using EmperialApps.WeatherSpark.Data;
using EmperialApps.WeatherSpark.Internal;
using EmperialApps.WeatherSpark.Resources;
using Microsoft.Phone.Controls;
using System.Net;
using System.Windows.Navigation;

namespace EmperialApps.WeatherSpark {

    public class ForecastPage : PhoneApplicationPage {

        private readonly ForecastDownloadManager _downloadManager;


        /// <summary>Initializes a new instance of the <see cref="ForecastPage"/> class.</summary>
        protected ForecastPage( ) {
            this.RegisterTranstions( );

            this._downloadManager = new ForecastDownloadManager( this, this.SetProgress );
            this._downloadManager.ForecastDownloadCompleted += ( s, e ) => this.OnForecastDownloadComplete( e );
        }


        protected override void OnNavigatedFrom( NavigationEventArgs e ) {
            base.OnNavigatedFrom( e );

            this.CancelForecastDownload( );
        }


        /// <summary>Gets the location <see cref="Forecast"/> was downloaded from.</summary>
        protected Coordinate? ForecastLocation {
            get {
                var settings = this._downloadManager.ForecastSettings;
                return settings != null
                     ? settings.Location
                     : default( Coordinate? );
            }
        }

        /// <summary>Gets the source the <see cref="Forecast"/> was downloaded from.</summary>
        protected ForecastSourceId? ForecastSourceId {
            get {
                var settings = this._downloadManager.ForecastSettings;
                return settings != null
                     ? settings.SourceId
                     : default( ForecastSourceId? );
            }
        }

        /// <summary>Gets the URI <see cref="Forecast"/> was downloaded from.</summary>
        protected string ForecastUri { get { return this._downloadManager.ForecastUri; } }

        /// <summary>Gets the last downloaded forecast.</summary>
        protected Forecast Forecast { get { return this._downloadManager.Forecast; } }

        /// <summary>Gets a value indicating whether a forecast is being downloaded.</summary>
        protected bool Downloading { get { return this._downloadManager.Downloading; } }

        /// <summary>Gets or sets a value indicating whether to save a downloaded forecast to a file.</summary>
        protected bool DisableForecastSave {
            get { return this._downloadManager.DisableForecastSave; }
            set { this._downloadManager.DisableForecastSave = value; }
        }


        /// <summary>Retrieves a new cancellable web client.</summary>
        protected WebClient GetWebClient( ) {
            return this._downloadManager.GetWebClient( );
        }

        /// <summary>Clears the specified client.</summary>
        protected void ReturnClient( object sender ) {
            this._downloadManager.ReturnClient( sender );
        }

        /// <summary>Called when a forecast download has completed.</summary>
        protected virtual void OnForecastDownloadComplete( ForecastEventArgs e ) { }

        /// <summary>Called to record download progress.</summary>
        internal virtual void SetProgress( PhoneApplicationPage page, ProgressKind kind, Localized.Status status, Localized.FormatArgument args ) {
            Extensions.SetProgress( page, kind, status, args );
        }

        /// <summary>Begins the download of the forecast at the specified URI.</summary>
        protected bool BeginForecastDownload( LocationSettings locationSettings, string forecastUri, DownloadReason reason ) {
            return this._downloadManager.BeginForecastDownload( locationSettings, forecastUri, reason );
        }

        /// <summary>Cancels the current forecast download, if any.</summary>
        protected void CancelForecastDownload( ) {
            this._downloadManager.Cancel( );
        }

    }

}
