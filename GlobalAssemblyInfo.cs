﻿
using System.Reflection;

[assembly: AssemblyCompany( "EmperialApps" )]
[assembly: AssemblyProduct( "WeatherSpark" )]
[assembly: AssemblyTitle( EmperialApps.WeatherSpark.AssemblyInfo.Title )]

[assembly: AssemblyVersion( EmperialApps.WeatherSpark.AssemblyInfo.Version )]
[assembly: AssemblyFileVersion( EmperialApps.WeatherSpark.AssemblyInfo.Version )]

#if DEBUG
[assembly: AssemblyConfiguration( "Debug" )]
#else
[assembly: AssemblyConfiguration( "Release" )]
#endif

namespace EmperialApps.WeatherSpark {

    internal static partial class AssemblyInfo {

        public const string Major = "4";
        public const string Minor = "4";
        public const string Version = Major + "." + Minor + "." + VersionInfo.Year + "." + VersionInfo.Date;

    }

}
