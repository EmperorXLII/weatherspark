﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a timer that runs at regular intervals.</summary>
    public interface ITimer {

        /// <summary>Occurs when the timer fires.</summary>
        event EventHandler Tick;

        /// <summary>Starts the timer on the specified interval.</summary>
        void Start( TimeSpan interval );

        /// <summary>Stops the timer.</summary>
        void Stop( );

    }

}
