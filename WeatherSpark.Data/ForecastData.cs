﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Indicates the data saved in a <see cref="Forecast"/>.</summary>
    [Flags]
    public enum ForecastData : ushort {
        None = 0,
        Temperature = 1 << 0,
#if DEBUG
        Unused1 = 1 << 1,
#endif
        PrecipitationPotential = 1 << 2,
#if DEBUG
        Unused3 = 1 << 3,
        Unused4 = 1 << 4,
        Unused5 = 1 << 5,
#endif
        SkyCover = 1 << 6,
        RelativeHumidity = 1 << 7,
        WindSustainedSpeed = 1 << 8,
        WindDirection = 1 << 9,
        Pressure = 1 << 10,

        Last = Pressure
    }

}
