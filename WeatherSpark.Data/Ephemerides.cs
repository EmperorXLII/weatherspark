﻿
using System;
using System.Collections.Generic;

namespace EmperialApps.WeatherSpark.Data {

    using DateRange = Pair<DateTimeOffset, DateTimeOffset>;

    /// <summary>Calculates astronomical positions and phases.</summary>
    public static class Ephemerides {

        /// <summary>Gets the Julian date for the specified local date.</summary>
        public static double GetJulianDate( DateTimeOffset date ) {
            DateTimeOffset offsetDate = date.AddDays( GregorianJulianOffset )
                .ToUniversalTime( );
            TimeSpan startDifference = offsetDate - JulianStart;
            return startDifference.TotalDays;
        }

        /// <summary>Gets the time of sunrise and sunset for the specified date.</summary>
        /// <remarks>Implementation of the sunrise equation.</remarks>
        /// <see href="http://en.wikipedia.org/wiki/Sunrise_equation"/>
        /// <see href="http://www.esrl.noaa.gov/gmd/grad/solcalc/sunrise.html"/>
        public static DateRange CalculateDaylight( Coordinate location, DateTimeOffset date ) {
            double latitude = location.Latitude;
            double longitude = location.Longitude;
            double timezoneOffset = date.Offset.TotalHours;

            double julianDay = GetJulianDate( date ) + 0.5;
            double julianCentury = (julianDay - JulianCenturyOffset) / JulianDaysPerYear;

            double solarMeanLongitude = (280.46646 + julianCentury * (36000.76983 + julianCentury * 0.0003032)) % ConvertValue.DegreesPerCircle;
            double solarMeanAnomaly = 357.52911 + julianCentury * (35999.05029 - 0.0001537 * julianCentury);
            double earthOrbitEccentricity = 0.016708634 - julianCentury * (0.000042037 + 0.0000001267 * julianCentury);
            double solarEquationOfCenter =
                      Sine( solarMeanAnomaly ) * (1.914602 - julianCentury * (0.004817 + 0.000014 * julianCentury))
                + Sine( 2 * solarMeanAnomaly ) * (0.019993 - 0.000101 * julianCentury)
                + Sine( 3 * solarMeanAnomaly ) * 0.000289;

            double solarTrueLongitude = solarMeanLongitude + solarEquationOfCenter;
            double solarApparentLongitude = solarTrueLongitude - 0.00569 - 0.00478 * Sine( 125.04 - 1934.136 * julianCentury );
            double meanObliqueEcliptic = 23 + (26 + ((21.448 - julianCentury * (46.815 + julianCentury * (0.00059 - julianCentury * 0.001813)))) / 60) / 60;
            double obliqueCorrection = meanObliqueEcliptic + 0.00256 * Cosine( 125.04 - 1934.136 * julianCentury );

            double solarDeclination = ArcSine( Sine( obliqueCorrection ) * Sine( solarApparentLongitude ) );
            double y = Math.Pow( Tangent( obliqueCorrection / 2 ), 2 );
            double equationOfTime =
                  4 * ConvertValue.ToDegrees( y * Sine( 2 * solarMeanLongitude )
                - 2 * earthOrbitEccentricity * Sine( solarMeanAnomaly )
                + 4 * earthOrbitEccentricity * Sine( solarMeanAnomaly ) * y * Cosine( 2 * solarMeanLongitude )
                - 0.5 * y * y * Sine( 4 * solarMeanLongitude )
                - 1.25 * earthOrbitEccentricity * earthOrbitEccentricity * Sine( 2 * solarMeanAnomaly ) );
            double sunriseHourAngle = ArcCosine( Cosine( 90.833 ) / (Cosine( latitude ) * Cosine( solarDeclination )) - Tangent( latitude ) * Tangent( solarDeclination ) );

            double solarNoon = (720 - 4 * longitude - equationOfTime + timezoneOffset * 60) / MinutesPerDay;
            double sunrise = (solarNoon * MinutesPerDay - sunriseHourAngle * 4) / MinutesPerDay;
            double sunset = (solarNoon * MinutesPerDay + sunriseHourAngle * 4) / MinutesPerDay;

            var daylight = new DateRange(
                date.AddDays( sunrise ),
                date.AddDays( sunset ) );
            return daylight;
        }


        /// <summary>Gets the phase of the moon for the specified date.</summary>
        /// <remarks>Practical Astronomy With Your Calculator, 3rd Edition; by Peter Duffett-Smith</remarks>
        public static double CalculateMoonPhase( DateTimeOffset date, out double age ) {
            double longitudeOffset, anomolyCorrection;
            CalculateMoonPosition( date, out longitudeOffset, out anomolyCorrection );

            //// Moon's phase (section 67, p.147)
            double D_moon = PositiveDegrees( longitudeOffset );
            double F = (1 - Cosine( D_moon )) / 2.0;

            age = D_moon / ConvertValue.DegreesPerCircle;
            return F;
        }

        /// <summary>Gets the rise and set of the moon over the specified date range.</summary>
        /// <remarks>Practical Astronomy With Your Calculator, 3rd Edition; by Peter Duffett-Smith</remarks>
        public static IEnumerable<DateRange> CalculateMoonTransits( Coordinate location, DateTimeOffset date, int days ) {
            double longitudeFactor = location.Longitude / 15;
            DateTimeOffset start = date.GetDateOffset( );
            DateTimeOffset end = start.AddDays( days );

            int limit = days;
            for( int day = -1; day < limit; ++day ) {
                DateTimeOffset moonrise, moonset;
                DateRange transit = CalculateMoonTransit( location, date.AddDays( day ) );
                transit.GetValues( out moonrise, out moonset );

                if( moonrise > end )
                    break;

                if( moonrise == moonset )
                    ++limit;
                else if( moonset >= start )
                    yield return transit;
            }
        }

        private static DateRange CalculateMoonTransit( Coordinate location, DateTimeOffset date ) {
            //// Moonrise and moonset (section 70, p.151)
            // 1,2: moon position references
            DateTimeOffset midnight = new DateTimeOffset( date.Date, TimeSpan.Zero );
            DateTimeOffset midday = midnight.AddHours( ConvertValue.HoursPerDay / 2 );
            double alpha_1, delta_1, alpha_2, delta_2, longitudeOffset, anomolyCorrection;
            CalculateMoonPosition( midday, out longitudeOffset, out anomolyCorrection ).GetValues( out alpha_2, out delta_2 );
            CalculateMoonPosition( midnight, out longitudeOffset, out anomolyCorrection ).GetValues( out alpha_1, out delta_1 );

            // 3,4: convert position to sidereal time (section 33, p.52)
            double GST1_r, GST1_s, GST2_r, GST2_s;
            GreenwichSiderealRiseAndSet( location, alpha_1, delta_1 ).GetValues( out GST1_r, out GST1_s );
            GreenwichSiderealRiseAndSet( location, alpha_2, delta_2 ).GetValues( out GST2_r, out GST2_s );

            if( GST2_r < GST1_r )
                return default( DateRange );

            // 5: sidereal time reference (section 12, p.17)
            double T = (GetJulianDate( midnight ) - JulianCenturyOffset) / JulianDaysPerYear;
            double T00 = PositiveHours( 6.697374558 + (2400.051336 + 0.000025862 * T) * T );

            // 8: interpolated rise and set times
            double GST_r = (12.03 * GST1_r - T00 * (GST2_r - GST1_r))
                         / (12.03 + GST1_r - GST2_r);
            double GST_s = (12.03 * GST1_s - T00 * (GST2_s - GST1_s))
                         / (12.03 + GST1_s - GST2_s);

            // 9: parallax/refraction/finite diameter corrections (section 33, p.52; section 37, p.64; section 69, p.150)
            double delta_prime = (delta_1 + delta_2) / 2;
            double rho_prime = (1 - e_moon * e_moon) / (1 + e_moon * Cosine( anomolyCorrection ));
            double pi = pi_0 / rho_prime;
            double theta = theta_0 / rho_prime;
            double x = -pi + theta / 2 + 34 / 60.0;
            double psi = ArcCosine( Sine( location.Latitude ) / Cosine( delta_prime ) );
            double y = ArcSine( Sine( x ) / Sine( psi ) );
            double delta_t = 240 * y / Cosine( delta_prime );

            // 10,11: convert to local time (section 10, p.15)
            double hours_delta_t = delta_t / SecondsPerHour;
            double UT_r = (GST_r - hours_delta_t - T00) * 0.9972695663;
            double UT_s = (GST_s + hours_delta_t - T00) * 0.9972695663;
            DateTimeOffset moonrise = (midnight + TimeSpan.FromHours( UT_r )).ToOffset( date.Offset );
            DateTimeOffset moonset = (midnight + TimeSpan.FromHours( UT_s )).ToOffset( date.Offset );

            System.Diagnostics.Debug.Assert( moonrise < moonset, "Moonrise " + moonrise + " after moonset " + moonset );
            return new DateRange( moonrise, moonset );
        }


        #region Private Members

        private const double MinutesPerDay = 1440;
        private const double SecondsPerHour = 3600;
        private const double JulianDaysPerYear = 36525.0;
        private const double JulianCenturyOffset = 2451545.0;
        private const double GregorianJulianOffset = 2415018.5;
        private static readonly DateTimeOffset JulianStart = new DateTimeOffset( ).AddDays( 693593 );
        private static readonly DateTimeOffset Epoch = new DateTimeOffset( 1990, 1, 1, 0, 0, 0, TimeSpan.Zero ).AddDays( -1 ); // January 0.0 1990

        private const double epsilon_g = 279.403303;    // (degrees) ecliptic longitude at epoch 1990.0
        private const double omega_bar_g = 282.768422;  // (degrees) ecliptic longitude of perigee
        private const double e_sun = 0.016713;          // eccentricity of orbit

        private const double l_0 = 318.351648;  // (degrees) Moon's mean longitude at the epoch
        private const double P_0 = 36.340410;   // (degrees) mean longitude of the perigee at the epoch
        private const double N_0 = 318.510107;  // (degrees) mean longitude of the node at the epoch
        private const double i_moon = 5.145396; // (degrees) inclination of the Moon's orbit
        private const double e_moon = 0.054900; // eccentricity of Moon's orbit
        private const double a_moon = 384401;   // (km) semi-major axis of Moon's orbit
        private const double theta_0 = 0.5181;  // (degrees) Moon's angular size at distance 'a' from the Earth
        private const double pi_0 = 0.9507;     // (degrees) parallax at distance 'a' from the Earth


        /// <summary>
        /// Gets the apparent rise and set times from a geographic location for an object at the specified celestial coordinates,
        /// in Greenwich Sidereal Time (section 33, p.52).
        /// </summary>
        private static Pair<double, double> GreenwichSiderealRiseAndSet( Coordinate location, double ascension, double declination ) {
            double hours = ArcCosine( -Tangent( location.Latitude ) * Tangent( declination ) ) / 15;
            double localSiderealRise = ascension - hours + ConvertValue.HoursPerDay;
            double localSiderealSet = ascension + hours;
            if( localSiderealRise > ConvertValue.HoursPerDay )
                localSiderealRise -= ConvertValue.HoursPerDay;

            double longitudeFactor = location.Longitude / 15;
            double siderealRise = localSiderealRise - longitudeFactor;
            double siderealSet = localSiderealSet - longitudeFactor;
            if( siderealSet < siderealRise )
                siderealSet += ConvertValue.HoursPerDay;

            return Pair.Create( siderealRise, siderealSet );
        }

        /// <summary>
        /// Calculates the right ascension and declination of the moon, and related factors (section 65, p.142).
        /// </summary>
        private static Pair<double, double> CalculateMoonPosition( DateTimeOffset date, out double longitudeOffset, out double anomolyCorrection ) {
            // 1,2: days since January 0.0 1990
            TimeSpan difference = date - Epoch;
            double D = difference.TotalDays;

            // 3: sun position (section 46, p.86)
            double N_sun = PositiveDegrees( (ConvertValue.DegreesPerCircle / 365.242191) * D );
            double M_sun = PositiveDegrees( N_sun + epsilon_g - omega_bar_g ); // mean anomaly
            double E_c_sun = (ConvertValue.DegreesPerCircle / Math.PI) * e_sun * Sine( M_sun );
            double lambda_sun = PositiveDegrees( N_sun + E_c_sun + epsilon_g ); // geocentric ecliptic longitude

            // 4,5,6: moon position
            double l = PositiveDegrees( 13.1763966 * D + l_0 );
            double M_m = PositiveDegrees( l - 0.1114041 * D - P_0 );
            double N = PositiveDegrees( N_0 - 0.0529539 * D );

            // 7,8: correction factors
            double C = l - lambda_sun;
            double E_v = 1.2739 * Sine( 2 * C - M_m );
            double A_e = 0.1858 * Sine( M_sun );
            double A_3 = 0.37 * Sine( M_sun );

            // 9,10,11: corrected anomaly
            double M_m_prime = M_m + E_v - A_e - A_3;
            double E_c = 6.2886 * Sine( M_m_prime );
            double A_4 = 0.214 * Sine( 2 * M_m_prime );

            // 12,13,14: corrected moon position
            double l_prime = l + E_v + E_c - A_e + A_4;
            double V = 0.6583 * Sine( 2 * (l_prime - lambda_sun) );
            double l_double_prime = l_prime + V; // true longitude

            // 15,16,17,18,19: moon ecliptic coordinates
            double N_prime = N - 0.16 * Sine( M_sun );
            double longitude_error = l_double_prime - N_prime;
            double y_moon = Sine( longitude_error ) * Cosine( i_moon );
            double x_moon = Cosine( longitude_error );
            double lambda_moon = ArcTangent( y_moon, x_moon ) + N_prime;
            double beta_moon = ArcSine( Sine( longitude_error ) * Sine( i_moon ) );

            // 20,21: moon equatorial coordinates (section 27, p.40)
            double T = (GetJulianDate( date ) - JulianCenturyOffset) / JulianDaysPerYear;
            double delta_epsilon = (46.815 + (0.0006 - 0.001817 * T) * T) * T;
            double epsilon = 23.439292 - delta_epsilon / SecondsPerHour;
            double delta = ArcSine(                                     // declination
                   Sine( beta_moon ) * Cosine( epsilon )
                 + Cosine( beta_moon ) * Sine( epsilon ) * Sine( lambda_moon ) );
            double y = Sine( lambda_moon ) * Cosine( epsilon )
                    - Tangent( beta_moon ) * Sine( epsilon );
            double x = Cosine( lambda_moon );
            double alpha = PositiveDegrees( ArcTangent( y, x ) ) / 15;  // ascension

            longitudeOffset = l_double_prime - lambda_sun;
            anomolyCorrection = M_m_prime + E_c;
            return Pair.Create(
                alpha,
                delta );
        }


        private static double Sine( double degrees ) {
            double radians = ConvertValue.ToRadians( degrees );
            return Math.Sin( radians );
        }

        private static double Cosine( double degrees ) {
            double radians = ConvertValue.ToRadians( degrees );
            return Math.Cos( radians );
        }

        private static double Tangent( double degrees ) {
            double radians = ConvertValue.ToRadians( degrees );
            return Math.Tan( radians );
        }

        private static double ArcSine( double sine ) {
            double radians = Math.Asin( sine );
            return ConvertValue.ToDegrees( radians );
        }

        private static double ArcCosine( double cosine ) {
            double constrained =
                cosine > +1.0 ? +1.0 :
                cosine < -1.0 ? -1.0 :
                cosine;
            double radians = Math.Acos( constrained );
            return ConvertValue.ToDegrees( radians );
        }

        private static double ArcTangent( double y, double x ) {
            double radians = Math.Atan2( y, x );
            return ConvertValue.ToDegrees( radians );
        }


        private static double PositiveDegrees( double degrees ) {
            return PositiveModulus( degrees, ConvertValue.DegreesPerCircle );
        }

        private static double PositiveHours( double hours ) {
            return PositiveModulus( hours, ConvertValue.HoursPerDay );
        }

        private static double PositiveModulus( double value, double modulus ) {
            double normalized = value % modulus;
            if( normalized < 0.0 )
                normalized += modulus;

            return normalized;
        }

        #endregion
    }

}
