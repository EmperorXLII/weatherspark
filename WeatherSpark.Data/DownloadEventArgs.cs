﻿
using System;


namespace EmperialApps.WeatherSpark.Data {

    public class ForecastEventArgs : DownloadEventArgs<Coordinate, Forecast> {

        public ForecastEventArgs( Coordinate location, Forecast forecast, Exception error = null )
            : base( location, forecast, error ) { }

        public Coordinate Location { get { return this.Key; } }

        public Forecast Forecast { get { return this.Item; } }

        public virtual ForecastEventArgs UpdateForecast( Forecast forecast ) {
            return new ForecastEventArgs( this.Location, forecast, this.Error );
        }

        public override string ToString( ) {
            return string.Format( "Location=({0}), Forecast=({1}), Error=({2})", this.Location, this.Forecast, this.Error );
        }

    }

    public sealed class LocationEventArgs : DownloadEventArgs<string, Place[]> {

        public LocationEventArgs( string search, Place[] places, Exception error = null )
            : base( search, places, error ) { }

        public string Search { get { return this.Key; } }

        public Place[] Places { get { return this.Item; } }

    }

}
