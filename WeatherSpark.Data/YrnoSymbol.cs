﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Values for the <c>symbol</c> field in yr.no forecast data.</summary>
    /// <seealso href="http://om.yr.no/forklaring/symbol/"/>
    public enum YrnoSymbol {
        ClearSky = 1,
        Fair = 2,
        PartlyCloudy = 3,
        Cloudy = 4,
        RainShowers = 5,
        RainShowersAndThunder = 6,
        SleetShowers = 7,
        SnowShowers = 8,
        Rain = 9,
        HeavyRain = 10,
        HeavyRainAndThunder = 11,
        Sleet = 12,
        Snow = 13,
        SnowAndThunder = 14,
        Fog = 15,
        ClearSkyAndDark = 16,
        PartlyCloudyAndDark = 17,
        RainShowersAndDark = 18,
        SnowShowersAndDark = 19,
        SleetShowersAndThunder = 20,
        SnowShowersAndThunder = 21,
        RainAndThunder = 22,
        SleetAndThunder = 23,
        LightRainShowersAndThunder = 24,
        HeavyRainShowersAndThunder = 25,
        LightSleetShowersAndThunder = 26,
        HeavySleetShowersAndThunder = 27,
        LightSnowShowersAndThunder = 28,
        HeavySnowShowersAndThunder = 29,
        LightRainAndThunder = 30,
        LightSleetAndThunder = 31,
        HeavySleetAndThunder = 32,
        LightSnowAndThunder = 33,
        HeavySnowAndThunder = 34,

        LightRainShowers = 40,
        HeavyRainShowers = 41,
        LightSleetShowers = 42,
        HeavySleetShowers = 43,
        LightSnowShowers = 44,
        HeavySnowShowers = 45,
        LightRain = 46,
        LightSleet = 47,
        HeavySleet = 48,
        LightSnow = 49,
        HeavySnow = 50,
    }

}
