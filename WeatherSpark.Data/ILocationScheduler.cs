﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a scheduler that saves locations to process.</summary>
    public interface ILocationScheduler {

        /// <summary>Adds the specified location to the queue.</summary>
        void AddLocation( Coordinate location );

        /// <summary>Tries to get the next scheduled location.</summary>
        bool TryGetNext( out Coordinate location );

        /// <summary>Schedules the specified location for later processing.</summary>
        void Schedule( Coordinate location, Coordinate newLocation );

        /// <summary>Removes the specified location from further scheduling.</summary>
        void Remove( Coordinate location );

    }

}
