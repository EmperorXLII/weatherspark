﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Contains extension methods for guarding arguments.</summary>
    public static class GuardExtensions {

        /// <summary>Gets a guard for the parameter with the specified name.</summary>
        public static Guard<T> GuardParam<T>( this T value, string name ) {
            return new Guard<T>( value, name );
        }

        /// <summary>Represents a guard for a parameter.</summary>
        public struct Guard<T> {
            public readonly T Value;
            public readonly string Name;

            public Guard( T value, string name ) {
                this.Value = value;
                this.Name = name;
            }

            public void NotNull( ) {
                if( this.Value == null )
                    throw new ArgumentNullException( this.Name );
            }

            public void IsInRange( bool inRange, string message ) {
                if( !inRange )
                    throw new ArgumentOutOfRangeException( this.Name, message );
            }
        }

    }

}
