﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a storage and retrieval system.</summary>
    public interface IStore<TKey, TItem, TEventArgs>
        where TItem : class
        where TEventArgs : DownloadEventArgs<TKey, TItem> {

        /// <summary>Saves an item to the store.</summary>
        bool Save( TEventArgs e );

        /// <summary>Loads an item from the store, or returns <see langword="null"/> if no item has been saved.</summary>
        TItem Load( TKey key, object context );


        /// <summary>Occurs when a download started by <see cref="BeginDownload"/> completes.</summary>
        event EventHandler<TEventArgs> DownloadCompleted;

        /// <summary>Retrieves the latest item for the specified key.</summary>
        void BeginDownload( TKey key );
    }

}
