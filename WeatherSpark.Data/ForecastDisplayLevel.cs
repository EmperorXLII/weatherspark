﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Indicates how much information to display about a <see cref="Forecast"/>.</summary>
    [Flags]
    public enum ForecastDisplayLevel {
        None = 0,
        Set = 1 << 0,

        TemperatureSummary = 1 << 1,
        TemperatureDetails = 1 << 2,
        TemperatureMask = TemperatureSummary | TemperatureDetails,

        HumiditySummary = 1 << 3,
        HumidityDetails = 1 << 4,
        HumidityMask = HumiditySummary | HumidityDetails,

        PrecipitationSummary = 1 << 5,
        PrecipitationDetails = 1 << 6,
        PrecipitationMask = PrecipitationSummary | PrecipitationDetails,

        CloudCoverSummary = 1 << 7,
        CloudCoverDetails = 1 << 8,
        CloudCoverMask = CloudCoverSummary | CloudCoverDetails,

        WindSummary = 1 << 9,
        WindDetails = 1 << 10,
        WindMask = WindSummary | WindDetails,

        PressureSummary = 1 << 11,
        PressureDetails = 1 << 12,
        PressureMask = PressureSummary | PressureDetails,

        MoonSummary = 1 << 13,

        SummaryMask = Set | TemperatureMask | HumiditySummary | PrecipitationMask | CloudCoverMask | WindSummary | PressureSummary,
        DetailsMask = Set | TemperatureMask | HumidityMask    | PrecipitationMask | CloudCoverMask | WindMask    | PressureMask    | MoonSummary
    }

}
