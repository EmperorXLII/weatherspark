﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Caches the values of the specified input stream as they are read.</summary>
    public class CachingStream : Stream {

        private readonly List<byte> _cache;
        private readonly Stream _input;
        private int? _position;

        public CachingStream( Stream input ) {
            this._cache = new List<byte>( capacity: (int)input.Length );
            this._input = input;
        }


        public override bool CanRead {
            get { return this._input.CanRead; }
        }

        public override bool CanSeek {
            get { return true; }
        }

        public override bool CanWrite {
            get { return false; }
        }

        public override long Length {
            get { return this._input.Length; }
        }

        public override long Position {
            get {
                return this._position
                    ?? this._input.Position;
            }

            set {
                if( value < this._cache.Count ) {
                    this._position = (int)value;
                }
                else {
                    this._position = null;

                    int unread = (int)(value - this._input.Position);
                    if( unread > 0 )
                        this.Read( new byte[unread], 0, unread );

                    System.Diagnostics.Debug.Assert( this._input.Position == value,
                        "Position was not updated to " + value + " after reading " + unread + " unread values from " + this );
                }
            }
        }


        public byte[] GetCachedBytes( ) {
            return this._cache.ToArray( );
        }

        public override string ToString( ) {
            return GetType( ).Name + ": Length=" + this.Length + ", Position=" + this.Position + ", Cache=" + this._cache.Count;
        }

        protected override void Dispose( bool disposing ) {
            base.Dispose( disposing );

            if( disposing )
                this._input.Dispose( );
        }

        public int PeekByte( ) {
            long originalPosition = this.Position;
            int result = this.ReadByte( );
            this.Position = originalPosition;
            return result;
        }

        public override int ReadByte( ) {
            int result;

            if( this._position.HasValue ) {
                result = this._cache[this._position.Value];
                ++this.Position;
            }
            else {
                result = base.ReadByte( );
            }

            return result;
        }

        public override int Read( byte[] buffer, int offset, int count ) {
            int totalRead = 0;

            if( this._position.HasValue && count > 0 ) {
                int position = this._position.Value;
                int available = this._cache.Count - position;
                int cacheRead = Math.Min( available, count );

                this._cache.CopyTo( position, buffer, offset, cacheRead );

                this.Position += cacheRead;
                totalRead += cacheRead;
                offset += cacheRead;
                count -= cacheRead;
            }

            int inputRead = this._input.Read( buffer, offset, count );
            totalRead += inputRead;

            if( offset == 0 && inputRead == buffer.Length )
                this._cache.AddRange( buffer );
            else if( count > 0 )
                this._cache.AddRange( buffer.Skip( offset ).Take( inputRead ) );

            return totalRead;
        }

        public override long Seek( long offset, SeekOrigin origin ) {
            long position;
            switch( origin ) {
                default:
                case SeekOrigin.Begin:
                    position = offset;
                    break;
                case SeekOrigin.Current:
                    position = this.Position + offset;
                    break;
                case SeekOrigin.End:
                    position = this.Length + offset;
                    break;
            }

            this.Position = position;
            return position;
        }


        #region Unsupported Members

        public override void Flush( ) {
            throw new NotSupportedException( );
        }

        public override void SetLength( long value ) {
            throw new NotSupportedException( );
        }

        public override void Write( byte[] buffer, int offset, int count ) {
            throw new NotSupportedException( );
        }

        #endregion

    }

}
