﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a storage and retrieval system for locations.</summary>
    public interface ILocationStore : IStore<string, Place[], LocationEventArgs> { }

}
