﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Indicates the major headings on a compass.</summary>
    public enum CompassDirection : byte {
        N,
        NNE,
        NE,
        ENE,
        E,
        ESE,
        SE,
        SSE,
        S,
        SSW,
        SW,
        WSW,
        W,
        WNW,
        NW,
        NNW,
    }

}
