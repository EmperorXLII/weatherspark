﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Contains all the mode information used to create a tile image.</summary>
    [Flags]
    public enum ForecastTileMode : ushort {
        WideBackedImperialHour = Unit_Imperial | Display_Hour | Display_Back | Display_Wide,
        WideBackedImperialDay = Unit_Imperial | Display_Day | Display_Back | Display_Wide,
        WideBackedMetricHour = Unit_Metric | Display_Hour | Display_Back | Display_Wide,
        WideBackedMetricDay = Unit_Metric | Display_Day | Display_Back | Display_Wide,

        BackedImperialHour = Unit_Imperial | Display_Hour | Display_Back,
        BackedImperialDay = Unit_Imperial | Display_Day | Display_Back,
        BackedMetricHour = Unit_Metric | Display_Hour | Display_Back,
        BackedMetricDay = Unit_Metric | Display_Day | Display_Back,

        WideImperialHour = Unit_Imperial | Display_Hour | Display_Wide,
        WideImperialDay = Unit_Imperial | Display_Day | Display_Wide,
        WideMetricHour = Unit_Metric | Display_Hour | Display_Wide,
        WideMetricDay = Unit_Metric | Display_Day | Display_Wide,

        ImperialHour = Unit_Imperial | Display_Hour,
        ImperialDay = Unit_Imperial | Display_Day,
        MetricHour = Unit_Metric | Display_Hour,
        MetricDay = Unit_Metric | Display_Day,


        Unit_Metric = Units.Metric,
        Unit_Imperial = Units.Imperial,

        Display_Day = 0,
        Display_Hour = 1 << 1,
        Display_Back = 1 << 2,
        Display_Wide = 1 << 3,
    }

}
