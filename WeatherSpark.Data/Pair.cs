﻿
namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a pair of items.</summary>
    public partial struct Pair<T1, T2> {

        /// <inheritdoc/>
        public override string ToString( ) {
            return this.Item1 + ", " + this.Item2;
        }

        /// <summary>Retrieves the values from the pair.</summary>
        public void GetValues( out T1 item1, out T2 item2 ) {
            item1 = this.Item1;
            item2 = this.Item2;
        }

    }

    /// <summary>Contains helper methods for working with pairs.</summary>
    public static class Pair {

        /// <summary>Creates a new pair from the specified items.</summary>
        public static Pair<T1, T2> Create<T1, T2>( T1 item1, T2 item2 ) {
            return new Pair<T1, T2>( item1, item2 );
        }

        /// <summary>Tries to retrieve the values from a pair.</summary>
        public static bool TryGetValues<T1, T2>( this object state, out T1 item1, out T2 item2 ) {
            var pair = state as Pair<T1, T2>?;
            bool success = pair.HasValue;
            if( success ) {
                pair.Value.GetValues( out item1, out item2 );
            }
            else {
                item1 = default( T1 );
                item2 = default( T2 );
            }

            return success;
        }

        /// <summary>Swaps the values of the specified pair of items.</summary>
        public static void Swap<T>( ref T item1, ref T item2 ) {
            T temp = item1;
            item1 = item2;
            item2 = temp;
        }

    }

}
