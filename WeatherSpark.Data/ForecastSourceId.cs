
namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents the available forecast sources.</summary>
    public enum ForecastSourceId : ushort {
        NOAA,
        YRNO,
    }

}
