﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a collection of data values distributed at a given interval.</summary>
    public class DataValues : ReadOnlyCollection<double> {

        private readonly int _interval;

        /// <summary>Initializes a new instance of the <see cref="DataValues"/> class with the specified values and interval.</summary>
        public DataValues( IList<double> values, int interval )
            : base( values ) {
            this._interval = interval;
        }

        /// <summary>Gets the interval between data values.</summary>
        public int Interval { get { return this._interval; } }

        /// <inheritdoc/>
        public override string ToString( ) {
            return string.Format( "{0}: Count={1}, Interval={2}", this.GetType( ).Name, this.Count, this.Interval );
        }

    }

}
