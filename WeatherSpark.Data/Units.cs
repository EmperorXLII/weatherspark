﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Indicates the set of units used to represent a value.</summary>
    public enum Units {
        Metric = 0,
        Imperial,
    }

}
