﻿
using System.Collections.Generic;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Represents a storage and retrieval system for forecasts.</summary>
    public interface IForecastStore : IStore<Coordinate, Forecast, ForecastEventArgs> {

        /// <summary>Retrieves the location of all stored forecasts.</summary>
        IEnumerable<Coordinate> GetStoredLocations( );

    }

}
