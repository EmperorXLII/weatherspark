﻿
using System;

namespace EmperialApps.WeatherSpark.Data {

    /// <summary>Indicates how a <see cref="Forecast"/> should be displayed in a fixed-width view.</summary>
    public enum ForecastDisplayMode : int {
        None = 0,
        Day,
        Hour,
    }

}
